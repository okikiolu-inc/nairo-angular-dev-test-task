import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { firstValueFrom } from 'rxjs';


export type City = {
	Lat: number;
	Lon: number;
	Country: string;
};

type HourlyWeatherDescription = {
	description: string,
	icon: string,
	id: number,
	main: string,
};

// each hour
export type Hourly = {
	clouds: number,
	dew_point: number,
	dt: number,
	feels_like: number,
	humidity: number,
	pop: number,
	pressure: number,
	temp: number,
	uvi: number,
	visibility: number,
	wind_deg: number,
	wind_gust: number,
	wind_speed: number,
	weather: HourlyWeatherDescription[],
};

// each day
export type Daily = {
	clouds: number,
	dew_point: number,
	dt: number,
	feels_like: {
		day: number,
		night: number, 
		eve: number, 
		morn: number,
	},
	humidity: number,
	moon_phase: number,
	moonrise: number,
	moonset: number,
	pop: number,
	pressure: number,
	sunrise: number,
	sunset: number,
	temp: {
		day: number,
		eve: number,
		max: number,
		min: number,
		morn: number,
		night: number,
	},
	uvi: number,
	wind_deg: number,
	wind_gust: number,
	wind_speed: number,
}

@Injectable({providedIn: 'root'})
export class WeatherForecastApiService {

	private _apiKey = '010721642521f31b0fbc8c3831d45951';
	// private _apiKey = '653428ac0a9fc712680bdf6f6ad330d4';

	constructor(private http: HttpClient) { }

	public async searchCity (city: string): Promise<City | null> {

		const endpoint = `https://api.openweathermap.org/geo/1.0/direct?q=${city}&limit=1&appid=${this._apiKey}`;

		const request = this.http.get<City[]>(endpoint);

		const data = await firstValueFrom(request);

		if (data.length === 0) {
			return null;
		}
		else {
			const rawCity: any = data[0];
			return {
				Lat: rawCity['lat'],
				Lon: rawCity['lon'],
				Country: rawCity['country'],
			};
		}
	}

	public async getHourlyWeather (city: City, step?: number, max?: number): Promise<Hourly[]> {
		const _step = step || 3;
		const _max = max || 8;

		const endpoint = `https://api.openweathermap.org/data/2.5/onecall?lat=${city.Lat}&lon=${city.Lon}&exclude=current,minutely,daily,alerts&appid=${this._apiKey}&units=metric`;
		const request = this.http.get<any>(endpoint);
		const data = await firstValueFrom(request);
		const hourly: Hourly[] = data['hourly'];

		const HourlyWeather: Hourly[] = [];

		for (let i = 0; i < hourly.length; i = i + _step) {
			HourlyWeather.push(hourly[i]);
			if (HourlyWeather.length == _max) {
				break;
			}
		}

		return HourlyWeather;
	}

	public async getDailyWeather (city: City, days?: number): Promise<Daily[]> {
		let _days = days || 7;

		const endpoint = `https://api.openweathermap.org/data/2.5/onecall?lat=${city.Lat}&lon=${city.Lon}&exclude=current,minutely,hourly,alerts&appid=${this._apiKey}&units=metric`;
		const request = this.http.get<any>(endpoint);
		const data = await firstValueFrom(request);
		const daily: Daily[] = data['daily'];

		if (_days <= 0) {
			return [];
		}
		else if (_days >= daily.length) {
			return daily;
		}
		else {
			return daily.slice(0, _days);
		}
	}
}