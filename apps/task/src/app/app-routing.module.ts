import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DailyWeatherComponent } from './daily-weather/daily-weather.component';
import { HourlyWeatherComponent } from './hourly-weather/hourly-weather.component';

const routes: Routes = [
	{ path: 'daily/:city', component: DailyWeatherComponent },
	{ path: 'hourly/:city', component: HourlyWeatherComponent },
	{ path: '**', redirectTo: '/hourly/', pathMatch: 'full' },
];

// configures NgModule imports and exports
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
