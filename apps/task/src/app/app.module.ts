import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { WeatherForecastServicesModule } from '@bp/weather-forecast/services';
import { StoreModule } from '@ngrx/store';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { FormsModule } from '@angular/forms';

import { DailyWeatherComponent } from './daily-weather/daily-weather.component';
import { HourlyWeatherComponent } from './hourly-weather/hourly-weather.component';
import { appReducer } from './app.store';

@NgModule({
	declarations: [AppComponent, DailyWeatherComponent, HourlyWeatherComponent],
	imports: [
		BrowserModule,
		FormsModule,
		AppRoutingModule,
		WeatherForecastServicesModule,
		StoreModule.forRoot({ app: appReducer })
	],
	providers: [],
	bootstrap: [AppComponent],
})
export class AppModule {}
