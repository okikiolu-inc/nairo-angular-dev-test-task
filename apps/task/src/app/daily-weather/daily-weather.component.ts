import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Observable, firstValueFrom } from 'rxjs';
import { Store } from '@ngrx/store';
import { updateCity, updateDailyWeather, updateInfoFound, AppState} from '../app.store';

import { WeatherForecastApiService, Daily } from '@bp/weather-forecast/services';

@Component({
  selector: 'bp-daily-weather',
  templateUrl: './daily-weather.component.html',
  styleUrls: ['./daily-weather.component.scss']
})
export class DailyWeatherComponent implements OnInit {

	appstate$: Observable<AppState>;

  infoFound = true;

	constructor(
    private weatherService: WeatherForecastApiService,
    private route: ActivatedRoute,
		private store: Store<{ app: AppState }>

  ) {
    this.appstate$ = this.store.select('app');
  }

	ngOnInit() {

    this.route.paramMap.subscribe(async (params) => {
      const cityName  = params.get('city');

      const appstate = await firstValueFrom(this.appstate$);

      // only fetch when the city name changes
      if (cityName != appstate.city) {
        this.updateDailyWeather([]);
      }

      this.store.dispatch(updateInfoFound({ infoFound: true }));


      if (cityName) {
        this.updateCity(cityName);
        const city = await this.weatherService.searchCity(cityName);
  
        if (city) {
          const daily = await this.weatherService.getDailyWeather(city);
          this.updateDailyWeather(daily);
        }
        else {
          this.infoFound = false;
          this.store.dispatch(updateInfoFound({ infoFound: false }));
        }
      }
    });

	}

  updateCity (city: string) {
    this.store.dispatch(updateCity({ city }));
  }

  updateDailyWeather (daily: Daily[]) {
    this.store.dispatch(updateDailyWeather({
      daily,
    }))
  }

  dtToCleanDate(dt: number): string {
    const days = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday']
    const date = new Date(dt * 1000);

    return days[ date.getDay() ];
    ;
  }

}
