import { ChangeDetectionStrategy, Component } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';

import { AppState } from './app.store';

@Component({
	selector: 'bp-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AppComponent {
	title = `Neil's Weather App`;

	searchValue = ``;

	appstate$: Observable<AppState>;

	constructor(
		private store: Store<{ app: AppState }>,
		private router: Router,
	) {
		this.appstate$ = this.store.select('app');
	}

	ngOnInit() {

	}

	onsearchClick () {
		this.router.navigate(['/hourly', this.searchValue]);
	}
}
