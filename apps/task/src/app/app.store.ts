import { createAction, createReducer, on, props } from '@ngrx/store';

import { Hourly, Daily } from '@bp/weather-forecast/services';

export const updateCity = createAction('updateCity', props<{ city: string; }>());
export const updateInfoFound = createAction('updateInfoFound', props<{ infoFound: boolean; }>());
export const updateDailyWeather = createAction('updateDailyWeather', props<{ daily: Daily[]; }>());
export const updateHourlyWeather = createAction('updateHourlyWeather', props<{ hourly: Hourly[]; }>());

export type AppState = {
    city: string,
    hourly: Hourly[],
    daily: Daily[],
    infoFound: boolean,
};

export const initialState: AppState = {
    city: '',
    hourly: [],
    daily: [],
    infoFound: true,
};

const _appReducer = createReducer(
    initialState,
    on(updateCity, (state, action) => {
        return {
            ...state,
            city: action.city,
        };
    }),
    on(updateInfoFound, (state, action) => {
        return {
            ...state,
            infoFound: action.infoFound,
        };
    }),
    on(updateDailyWeather, (state, action) => {
        return {
            ...state,
            daily: action.daily,
        }
    }),
    on(updateHourlyWeather, (state, action) => {
        return {
            ...state,
            hourly: action.hourly,
        }
    })
);

export function appReducer(state: AppState | undefined, action: any) {
  return _appReducer(state, action);
}