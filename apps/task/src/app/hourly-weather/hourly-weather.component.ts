import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Observable, firstValueFrom } from 'rxjs';
import { Store } from '@ngrx/store';
import { updateCity, updateHourlyWeather, updateInfoFound, AppState} from '../app.store';

import { WeatherForecastApiService, Hourly } from '@bp/weather-forecast/services';


@Component({
  selector: 'bp-hourly-weather',
  templateUrl: './hourly-weather.component.html',
  styleUrls: ['./hourly-weather.component.scss']
})
export class HourlyWeatherComponent implements OnInit {

	appstate$: Observable<AppState>;

	constructor(
    private weatherService: WeatherForecastApiService,
    private route: ActivatedRoute,
		private store: Store<{ app: AppState }>

  ) {
    this.appstate$ = this.store.select('app');
  }

	ngOnInit() {
    this.route.paramMap.subscribe(async (params) => {
      const cityName = params.get('city');

      const appstate = await firstValueFrom(this.appstate$);

      if (cityName != appstate.city) {
        this.updateHourlyWeather([]);
      }

      this.store.dispatch(updateInfoFound({ infoFound: true }));

      if (cityName) {
        this.updateCity(cityName);

        const city = await this.weatherService.searchCity(cityName);
  
        if (city) {
          const hourly = await this.weatherService.getHourlyWeather(city);
          this.updateHourlyWeather(hourly);
        }
        else {
          this.store.dispatch(updateInfoFound({ infoFound: false }));
        }
      }
    });

	}

  updateCity (city: string) {
    this.store.dispatch(updateCity({ city }));
  }

  updateHourlyWeather (hourly: Hourly[]) {
    this.store.dispatch(updateHourlyWeather({
      hourly,
    }))
  }

  dtToCleanTime(dt: number): string {
    const date = new Date(dt * 1000);
    const hours = date.getHours();
    const minutes = "0" + date.getMinutes();

    const formattedTime = hours + ':' + minutes.substr(-2);

    return formattedTime;
  }

}
